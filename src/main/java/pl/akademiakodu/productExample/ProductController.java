package pl.akademiakodu.productExample;/*
    Request HTTP = adres URL + metoda HTTP
    standardowo przeglądarka wywołuje metodę GET
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductController {

    /*
        Jeśli użytkownik wpiszę w przeglądarce:
        http://localhost:8080
        to w tym momencie serwer uruchomi metodę add z
        klasy pl.akademiakodu.productExample.ProductController
     */

    // Dokładność jest kluczowa.

    @GetMapping("/")
    public String add(){
        return "form.html";
        // wyświetl plik html: resources/templates/form.html
    }


    /*
        @RequestParam, że zmienna z parametru URL zostaje zamieniona na zmienną Javową:
       /create?name=Pralka&description=super+pralka
     */
    @GetMapping("/create")
    public String create(
            @RequestParam String name, @RequestParam String description,
            ModelMap map
    ){
        // new Product("Pralka","super pralka");
        Product product = new Product(name,description);
        // "product" - nazwa zmiennej w html będzie widoczna jako zmienna lokalna product
        map.put("product",product);
        return "show"; // to samo jakbyśmy napisali show.html, ale krócej:)
    }





}
