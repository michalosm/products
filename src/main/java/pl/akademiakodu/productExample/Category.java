package pl.akademiakodu.productExample;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private Integer id;

    private String name;

    /*
        Zmienna czy też metoda statyczna jest to taka
        metoda/zmienna, która jest taka sama dla wszystkich obiektów

     */

    private static List<Category> categoryList = new ArrayList<>();

    // służy do inicjalizacji zmiennych statycznych
    static {
        categoryList.add(new Category(1,"Sport"));
        categoryList.add(new Category(2,"Elektronika"));
        categoryList.add(new Category(3,"AGD"));
        categoryList.add(new Category(4,"Obuwie"));
    }

    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Category> getCategoryList() {
        return categoryList;
    }
}
